import java.util.Objects;

public class hot_sauce implements sauce {

    public int spiciness;

    @Override
    public int getPrice() {
        return 7;
    }

    @Override
    public void initSpiciness() {
        spiciness = 3;
    }

    public int add_crunch(crunch.thin_crunch crunch) {
        return getPrice() + crunch.price;
    }

    public int add_crunch(crunch.thick_crunch crunch) {
        return getPrice() + crunch.price;
    }

    @Override
    public boolean equals(Object o) {
        return true;
    }
}
